import java.util.Scanner;

public class Jogo {

    Scanner scanner = new Scanner(System.in);

    public void executarJogo(Baralho baralho) {

        int somaCartas = 0;
        int continuarJogando = 0;
        boolean finaliza = true;

        System.out.println("Jogo iniciado, essas são suas duas primeiras cartas:");

        for (int i = 1; i <= 2; i++) {
            Carta cartaInicial = new Carta();
            cartaInicial = baralho.sortearCarta();
            System.out.println("- " + cartaInicial.getNumero() + " de " + cartaInicial.getNaipe());
            somaCartas += cartaInicial.getPesoNumero();
        }

        while (finaliza) {
            System.out.println();
            if (somaCartas > 21) {
                System.out.println("VOCÊ PERDEU! O valor da soma é igual a " + somaCartas + ", e ultrapassou 21...");
                System.out.println("Boa sorte na próxima =)");
                System.out.println();
                System.out.println();
                System.out.println();
                finaliza = false;
            } else {
                System.out.println("O valor da soma das suas cartas é igual a " + somaCartas + ", deseja continuar jogando?");
                System.out.print("Digite 1 para SIM ou 0 para NÃO: ");

                continuarJogando = scanner.nextInt();

                if (continuarJogando == 1) {
                    Carta cartaSeguinte = new Carta();
                    cartaSeguinte = baralho.sortearCarta();
                    System.out.println();
                    System.out.println("- " + cartaSeguinte.getNumero() + " de " + cartaSeguinte.getNaipe());
                    somaCartas += cartaSeguinte.getPesoNumero();
                } else {
                    System.out.println();
                    System.out.println("Seu resultado final foi " + somaCartas + "! Parabéns!");
                    System.out.println();
                    System.out.println();
                    System.out.println();
                    finaliza = false;
                }
            }

        }
    }
}