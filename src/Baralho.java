import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Baralho {

    List<Carta> baralho = new ArrayList<>();

    public void montarBaralho() {

        Carta carta;

        System.out.println("*****************************************************");
        System.out.println("Inciando processo de criação do baralho virtual...");

        for (NaipeEnum naipe : NaipeEnum.values()) {
            for (NumeroEnum numero : NumeroEnum.values()) {
                carta = new Carta();

                carta.setNumero(numero.name());
                carta.setPesoNumero(numero.pesoNumero);
                carta.setNaipe(naipe.name());
                carta.setPesoNaipe(naipe.pesoNaipe);

                baralho.add(carta);
            }
        }

        Collections.shuffle(baralho);

        System.out.println("Baralho criado com sucesso!!!");

//        for(Carta cart : listaDeCartas) {
//            listaDeCartas.remove(cart);
//            System.out.println(cart.getNaipe());
//        }
    }

    public Carta sortearCarta () {
        Carta carta = new Carta();
        carta = baralho.get(0);
        baralho.remove(carta);
        return carta;
    }

}
