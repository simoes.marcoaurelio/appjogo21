import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Baralho baralho = new Baralho();
        Jogo jogo = new Jogo();
        Scanner scanner = new Scanner(System.in);
        boolean novoJogo = true;

        System.out.println("*****************************************************");
        System.out.println("********************** JOGO 21 **********************");
        System.out.println("*****************************************************");

        int continua = 0;

        while (novoJogo) {
            System.out.println("Deseja iniciar um novo jogo?");
            System.out.print("Digite 1 para SIM ou 0 para NÃO: ");

            continua = scanner.nextInt();

            System.out.println();

            if(continua == 1) {
                baralho.montarBaralho();

                System.out.println();

                jogo.executarJogo(baralho);
            } else {
                System.out.println("Até a próxima!!");
                novoJogo = false;
            }
        }
    }
}