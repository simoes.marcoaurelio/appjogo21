//Classe não utilizada

import java.util.Random;

public class NaipeOld {

    Random random = new Random();

    private String naipe;
    private int referenciaNaipe;

    public String getNaipe() {
        return naipe;
    }

    public int getReferenciaNaipe() {
        return referenciaNaipe;
    }

    public String sortearNaipe() {
        int quantidadeDeNaipes = 4;

        referenciaNaipe = random.nextInt(quantidadeDeNaipes) + 1;

        switch (referenciaNaipe) {
            case 1:
                naipe = "Ouros";
                break;
            case 2:
                naipe = "Espadas";
                break;
            case 3:
                naipe = "Copas";
                break;
            case 4:
                naipe = "Paus";
                break;
        }
        return naipe;
    }

}
