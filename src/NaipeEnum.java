public enum NaipeEnum {
    OUROS(1),
    ESPADAS(2),
    COPAS(3),
    PAUS(4);

    int pesoNaipe;

    NaipeEnum(int pesoNaipe) {
        this.pesoNaipe = pesoNaipe;
    }
}