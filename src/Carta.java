public class Carta {

    private String numero;
    private int pesoNumero;
    private String naipe;
    private int pesoNaipe;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getPesoNumero() {
        return pesoNumero;
    }

    public void setPesoNumero(int pesoNumero) {
        this.pesoNumero = pesoNumero;
    }

    public String getNaipe() {
        return naipe;
    }

    public void setNaipe(String naipe) {
        this.naipe = naipe;
    }

    public int getPesoNaipe() {
        return pesoNaipe;
    }

    public void setPesoNaipe(int pesoNaipe) {
        this.pesoNaipe = pesoNaipe;
    }
}